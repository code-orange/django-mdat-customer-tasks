from django.core.management.base import BaseCommand

from django_mdat_customer_tasks.django_mdat_customer_tasks.tasks import (
    mdat_customer_item_price_seed,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_customer_item_price_seed()
