from celery import shared_task

from django_mdat_customer.django_mdat_customer.models import MdatItems, MdatItemPrices


@shared_task(name="mdat_customer_item_price_seed")
def mdat_customer_item_price_seed():
    items_without_price = MdatItems.objects.filter(mdatitemprices__isnull=True)

    for item in items_without_price:
        price = MdatItemPrices(
            item=item,
            price=0,
        )
        price.save()

    return


@shared_task(name="mdat_customer_item_fix_link")
def mdat_customer_item_fix_link():
    items_linked_to_placeholder = MdatItems.objects.filter(linked_to_id=1)

    for item in items_linked_to_placeholder:
        item.linked_to_id = item.id
        item.save()

    return
